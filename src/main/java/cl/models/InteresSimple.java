/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.models;

public class InteresSimple {

    private int capital;
    private double tasa;
    private int anos;

    public InteresSimple(int capital, double tasa, int anos) {
        this.capital = capital;
        this.tasa = tasa;
        this.anos = anos;
    }

    public int resultado() {
        //Formula I = C * (i/100) * n
        return (int) (this.capital * (this.tasa / 100) * this.anos);
    }

}
